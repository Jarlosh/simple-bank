from typing import Dict, List, Union

from rest_framework.response import Response
from rest_framework.views import exception_handler as drf_exception_handler


def make_response_data(status_code, result, addition=None, description=None):
    # following specific format
    return {
        'status': status_code,
        'result': result,
        'addition': addition or {},
        'description': description or {}
    }

def make_error_obj(title=None, detail=None):
    error = {}
    if title:
        error['title'] = title
    if detail:
        error['detail'] = detail
    return error

def parse_errors(errors: Dict[str, Union[List[str], str]]):
    """
    Format drf errors as json:api specified list of error objects
    """
    if isinstance(errors, dict):
        error_objects = []
        for key, detail in errors.items():
            if isinstance(detail, (list, tuple)):
                for error in detail:
                    error_objects.append(make_error_obj(key, error))
            else:
                error_objects.append(make_error_obj(key, detail))
        return error_objects
    else:
        return errors

def wrap_error_response(response, context):
    description = {}
    request = context.get('request', None)
    if request:
        description['request_data'] = request.data
    if not response:
        status_code = 500
    else:
        status_code = response.status_code
        description['errors'] = parse_errors(response.data)

    result = make_response_data(
        status_code=status_code,
        result=False,
        addition={},
        description=description
    )
    response.data = result
    return response

def exception_handler(exc, context):
    response = drf_exception_handler(exc, context)
    return wrap_error_response(response, context)










