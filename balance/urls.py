from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'accounts', views.BankAccountViewSet, basename='account')
router.register(r'persons', views.PersonViewSet)

app_name = 'balance'
urlpatterns = [
    path('api/', include(
        [
            path('ping/', views.PingAPIView.as_view()),
            path('', include(router.urls)),
        ]
    )),

]
