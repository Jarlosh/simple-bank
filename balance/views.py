from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView
from . import serializers
from .models import BankAccount, Person
from .utils import exception_handler, make_response_data

HTTP_OK = 200

class PingAPIView(APIView):
    """ Reports if service is available """

    def is_service_available(self):
        return True

    def get(self, request, format=None):
        is_available = self.is_service_available()
        data = make_response_data(
            HTTP_OK, result=is_available)
        return Response(data)

def can_subtract(bank_account, subtract_sum):
    balance, hold = bank_account.balance, bank_account.hold
    return balance - hold >= subtract_sum

class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = serializers.HyperPersonSerializer

class BankAccountViewSet(viewsets.ModelViewSet):
    queryset = BankAccount.objects.all()
    serializer_class = serializers.HyperBankAccountSerializer

    def get_exception_handler(self):
        return exception_handler

    def can_subtract(self, bank_account, subtract_sum):
        balance, hold = bank_account.balance, bank_account.hold
        return balance - hold >= subtract_sum

    @action(detail=True, methods=['POST'])
    def add(self, request, **kwargs):
        """ Add money to bank account """
        serializer = serializers.TransactionSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            transaction = serializer.save()
            amount = transaction.amount

            account = self.get_object()
            if account.is_open:
                account.balance += amount
                account.save()
                result = True
            else:
                raise APIException('Account is closed', 400)

            serializer = self.get_serializer(account)
            data = make_response_data(
                HTTP_OK, result,
                addition=serializer.data,
                description={'request_data': request.data}
            )
            return Response(data)

    @action(detail=True, methods=['POST'])
    def subtract(self, request, **kwargs):
        """ Subtract money from bank account """
        serializer = serializers.TransactionSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            transaction = serializer.save()
            amount = transaction.amount
            account = self.get_object()
            if account.is_open:
                if can_subtract(account, amount):
                    account.hold += amount
                    account.save()
                    result = True
                else:
                    raise APIException('Account is closed', 400)
            else:
                raise APIException('Balance is too low', 400)
            serializer = self.get_serializer(account)

            data = make_response_data(
                HTTP_OK, result,
                addition=serializer.data,
                description={'request_data': request.data}
            )
            return Response(data)

    @action(detail=True, methods=['GET'])  # alias for detail action
    def status(self, request, *args, **kwargs):
        """ Get bank account status """
        response = self.retrieve(request, *args, **kwargs)
        data = make_response_data(
            response.status_code, result=True, addition=response.data)
        response.data = data
        return response











