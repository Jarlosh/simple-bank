from rest_framework import serializers
from .models import BankAccount, Person

class HyperBankAccountSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='balance:account-detail'
    )

    person = serializers.HyperlinkedRelatedField(
        view_name='balance:person-detail',
        queryset=Person.objects.all()
    )

    class Meta:
        model = BankAccount
        fields = ['uuid', 'balance', 'hold', 'is_open', 'url', 'person']

class HyperPersonSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='balance:person-detail'
    )

    class Meta:
        model = Person
        fields = '__all__'

class BankTransaction:
    def __init__(self, amount):
        self.amount = amount


class TransactionSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=0)

    def update(self, instance, validated_data):
        instance.amount = validated_data.get('amount', instance.amount)
        return instance

    def create(self, validated_data):
        return BankTransaction(**validated_data)
