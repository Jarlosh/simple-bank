from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext as _
NAME_LEN = 32
name_validator = RegexValidator(r'^([а-яА-Яa-zA-Z])+$')

class Person(models.Model):
    last_name = models.TextField(
        _('фамилия'),
        max_length=NAME_LEN,
        validators=[name_validator]
    )

    first_name = models.TextField(
        _('имя'),
        max_length=NAME_LEN,
        validators=[name_validator]
    )

    middle_name = models.TextField(
        _('отчество'),
        max_length=NAME_LEN,
        validators=[name_validator]
    )


class BankAccount(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
    )

    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name='accounts',
    )

    balance = models.IntegerField(_('balance'))
    hold = models.IntegerField(_('hold'))
    is_open = models.BooleanField(_('open'))




