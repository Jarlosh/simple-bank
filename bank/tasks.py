from celery import shared_task

@shared_task
def process_transactions():
    processed = 0
    flow = 0
    try:
        from balance.models import BankAccount
        queryset = BankAccount.objects.filter(hold__gt=0)
        for account in queryset:
            processed += 1
            flow += account.hold
            account.balance -= account.hold
            account.hold = 0
            account.save()
    except:
        return 1
    else:
        print(f'processed {processed} accounts',
              f'flow', flow)
        return 0

