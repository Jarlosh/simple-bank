FROM alpine:latest

ENV PYTHONUNBUFFERED 1
ENV PYTHONIOENCODING=utf-8

RUN apk add --update --no-cache\
    gcc\
    musl-dev\
    python3-dev\
    py3-pip\
    postgresql-libs\
    postgresql-dev

RUN pip3 install pip --upgrade

WORKDIR /home/app

ADD ./requirements.txt /home/app/requirements.txt

RUN pip3 install -r requirements.txt

ADD . /home/app/
