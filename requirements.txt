django==3.0.2
djangorestframework==3.11.0
celery==4.4.0
redis==3.4.1
psycopg2==2.8.4
